import datetime


# TODO: Change to model
# Data cache is good idea
class GithubRepo(object):
    """Object to store and validate github repo information"""

    def __init__(self, fullName, description, cloneUrl, stars, createdAt):
        self.fullName = fullName
        self.description = description
        # We can validate cloneUrl but in my opinion it's to heavy.
        self.cloneUrl = cloneUrl
        self.stars = stars
        self.createdAt = createdAt

    @staticmethod
    def fromJson(mapping):
        """Parse GithubRepo object from json string with map, we use other variable names"""
        if mapping is None:
            return None

        # Null if is None
        return GithubRepo(
            mapping.get('full_name'),
            mapping.get('description'),
            mapping.get('clone_url'),
            mapping.get('stargazers_count'),
            mapping.get('created_at'),
        )
