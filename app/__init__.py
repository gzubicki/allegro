from flask import Flask
from settings.default_settings import configure_app

app = Flask(__name__)

configure_app(app)

from app import views
