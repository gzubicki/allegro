import os


class Config:
    REPOS_URL = 'https://api.github.com/repos/{}/{}'
    TOKEN = os.environ.get('TOKEN')
    GITHUB_TIMEOUT = os.environ.get('GITHUB_TIMEOUT', 10)


class ProductionConfig(Config):
    SECRET_KEY = 'dS34DfkjklSDSdsfasdfasdjlkdsfu'
    DEBUG = False
    TESTING = False
    ENV = 'production'
    SESSION_COOKIE_NAME = 'github_proxy'
    SERVER_NAME = os.environ.get('SERVER_NAME', None)


class DevConfig(Config):
    SECRET_KEY = 'dSDfkjklSDSjlbfsasdf32kdsfu'
    DEBUG = True
    TESTING = True
    ENV = 'development'
#     SESSION_COOKIE_NAME = 'github_proxy'


config = {
    "prod": "settings.default_settings.ProductionConfig",
    "dev": "settings.default_settings.DevConfig",
}


def configure_app(app):
    config_name = os.getenv("FLASK_CONFIGURATION", "dev")
    app.config.from_object(config[config_name])
