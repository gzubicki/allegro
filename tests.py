import unittest
from unittest.mock import patch, Mock
import requests
import json
from app import app
from app.views import GitHubAPI, from_json
from app.models import GithubRepo
from requests import Session


class TestIntegrations(unittest.TestCase):
    """
    Tests for api proxy.
    It's  naughty to hit github, i prefer mock
    """

    def setUp(self):
        app.config['TESTING'] = True
        self.app = app.test_client()
        self.timeout = app.config['GITHUB_TIMEOUT']

    def test_githubrepo_object(self):
        """Object creation test"""
        o1 = GithubRepo(
             fullName="django/django",
             description="The Web framework for perfectionists with deadlines.",
             cloneUrl="https://github.com/django/django.git",
             stars=47374,
             createdAt="2012-04-28T02:47:18Z"
        )

        self.assertEqual(o1.fullName, 'django/django')
        self.assertEqual(o1.cloneUrl, 'https://github.com/django/django.git')
        self.assertEqual(o1.stars, 47374)
        self.assertEqual(o1.createdAt, "2012-04-28T02:47:18Z")

    @patch.object(Session, 'get')
    def test_good_response(self, mock_get):
        """Test our url with mock session.get """
        # Correctyly mocked session should to return only 1 star ;)
        good_answer = b'{"fullName": "django/django", "description": "The Web framework for perfectionists with deadlines.", "cloneUrl": "https://github.com/django/django.git", "stars": 1, "createdAt": "2012-04-28T02:47:18Z"}'

        # Set correct data like github
        mock_get.return_value.status_code = 200
        mock_get.return_value.text = json.dumps({
             "full_name": "django/django",
             "description": "The Web framework for perfectionists with deadlines.",
             "clone_url": "https://github.com/django/django.git",
             "stargazers_count": 1,
             "created_at": "2012-04-28T02:47:18Z"
        })

        # call http://localhost:xxxx/repositories/param1/param2
        resp = self.app.get('repositories/x/django')

        # check with data was send to fake github repos/param1/param2 (ttps://api.github.com/repos/x/django')
        github_url = app.config['REPOS_URL'].format('x', 'django')
        self.assertIn(unittest.mock.call(github_url, timeout=self.timeout), mock_get.call_args_list)

        # we call only with url
        self.assertEqual(len(mock_get.call_args_list), 1)

        # Our api should to retrun 200
        self.assertEqual(resp.status_code, 200)

        # Our api should return maped objects
        self.assertEqual(resp.data, good_answer)

    @patch.object(Session, 'get')
    def test_404_response(self, mock_get):
        """Test our url with mock session.get """

        mock_get.return_value.status_code = 404

        # call http://localhost:xxxx/repositories/param1/param2
        resp = self.app.get('repositories/x/django')

        # check with data was send to fake github repos/param1/param2 (ttps://api.github.com/repos/x/django')
        github_url = app.config['REPOS_URL'].format('x', 'django')
        self.assertIn(unittest.mock.call(github_url, timeout=self.timeout), mock_get.call_args_list)

        # we call only with url
        self.assertEqual(len(mock_get.call_args_list), 1)

        self.assertEqual(resp.status_code, 404)

    @patch.object(Session, 'get')
    def test_401_response(self, mock_get):
        """Test our url with mock session.get """

        mock_get.return_value.status_code = 401

        # call http://localhost:xxxx/repositories/param1/param2
        resp = self.app.get('repositories/x/django')

        # check with data was send to fake github repos/param1/param2 (ttps://api.github.com/repos/x/django')
        github_url = app.config['REPOS_URL'].format('x', 'django')
        self.assertIn(unittest.mock.call(github_url, timeout=self.timeout), mock_get.call_args_list)

        # we call only with url
        self.assertEqual(len(mock_get.call_args_list), 1)

        # We map it to 403
        self.assertEqual(resp.status_code, 403)


if __name__ == "__main__":
    unittest.main()
